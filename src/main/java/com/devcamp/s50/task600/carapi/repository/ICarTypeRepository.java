package com.devcamp.s50.task600.carapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task600.carapi.models.CCarType;

public interface ICarTypeRepository extends JpaRepository<CCarType, Long>{

}
