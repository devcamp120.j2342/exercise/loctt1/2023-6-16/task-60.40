package com.devcamp.s50.task600.carapi.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "car")
public class CCar {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private int car_id;

     @Column(name = "car_code", unique = true)
     private String carCode;

     @Column(name = "car_name")
     private String carName;

     @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
     private Set<CCarType> carTypes;


     public CCar() {
     }


     public CCar(int id, String carCode, String carName, Set<CCarType> carTypes) {
          this.car_id = id;
          this.carCode = carCode;
          this.carName = carName;
          this.carTypes = carTypes;
     }


     public int getId() {
          return car_id;
     }


     public void setId(int id) {
          this.car_id = id;
     }


     public String getCarCode() {
          return carCode;
     }


     public void setCarCode(String carCode) {
          this.carCode = carCode;
     }


     public String getCarName() {
          return carName;
     }


     public void setCarName(String carName) {
          this.carName = carName;
     }


     public Set<CCarType> getCarTypes() {
          return carTypes;
     }


     public void setCarTypes(Set<CCarType> carTypes) {
          this.carTypes = carTypes;
     }

     
     
}
