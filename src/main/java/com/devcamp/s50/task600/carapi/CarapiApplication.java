package com.devcamp.s50.task600.carapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarapiApplication.class, args);
	}

}
