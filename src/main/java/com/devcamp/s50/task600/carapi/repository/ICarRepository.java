package com.devcamp.s50.task600.carapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task600.carapi.models.CCar;

public interface ICarRepository extends JpaRepository<CCar, Long>{
     CCar findByCarCode(String carCode);
}
