package com.devcamp.s50.task600.carapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task600.carapi.models.CCar;
import com.devcamp.s50.task600.carapi.models.CCarType;
import com.devcamp.s50.task600.carapi.repository.ICarRepository;
import com.devcamp.s50.task600.carapi.repository.ICarTypeRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class CarController {
     @Autowired
     ICarRepository pCarRepository;

     @Autowired
     ICarTypeRepository pCarTypeRepository;

     @GetMapping("/cars")
     public ResponseEntity<List<CCar>> getAllCar() {
        try {
            List<CCar> pCar = new ArrayList<CCar>();

            pCarRepository.findAll().forEach(pCar::add);

            return new ResponseEntity<>(pCar, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/car_type")
    public ResponseEntity<Set<CCarType>>getCarTypeByCarCodEntity(@RequestParam(value = "carCode") String carCode) {
        try {
            CCar vCar = pCarRepository.findByCarCode(carCode);
            
            if(vCar != null) {
            	return new ResponseEntity<>(vCar.getCarTypes(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
